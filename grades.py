import pandas as pd
import time
import re
import unidecode

if __name__ == '__main__':
    start = time.time()
    path = '/Users/michaelsilva/Desktop/grades.txt'
    content_pattern = '------------------------------------------------------------------------------------------------'

    print("start: ", time.strftime('%H:%M:%S', time.localtime()))

    file = open(path, encoding="utf8")
    try:
        print("read file: ", (time.time() - start), " seconds")
        code_pattern = '(\d{2}/\d{7})'

        last_user = None
        students = {}

        with open(path, 'r') as f:
            for line in f:
                if last_user is not None:
                    students[last_user]['grade'] = "".join(re.findall("[a-zA-Z]+", line))
                    students[last_user]['absences'] = "".join(filter(lambda i: i.isdigit(), line))
                    last_user = None
                else:
                    match = re.search(code_pattern, line)

                    if match:
                        name = " ".join(re.findall("[a-zA-Z]+", line))
                        code = "".join(filter(lambda i: i.isdigit(), match.group(1)))
                        last_user = unidecode.unidecode(re.sub(r"\s+", "", name, flags=re.UNICODE)).upper()

                        students[last_user] = {
                            'name_code': last_user,
                            'name': name,
                            'code': code,
                        }
        df = pd.DataFrame(students.values(), columns=['name_code', 'name', 'code', 'grade', 'absences'])
        df.to_csv('/Users/michaelsilva/Desktop/grades.csv', index=False)

    finally:
        file.close()

    print("TOTAL TIME: ", str(time.time() - start), " seconds")

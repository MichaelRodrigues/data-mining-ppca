moodle_1_events = {
    'Algum conteúdo foi publicado.',
    'Comentário criado',
    'Comentário visualizado',
    'Curso visto',
    'Discussão criada',
    'Discussão visualizada',
    'Formulário de notas visualizado',
    'Módulo de curso criado',
    'Módulo do curso visualizado',
    'Post criado',
    'Um envio foi submetido.'
    'Submissão criada.',
    'Um arquivo foi enviado.',
    'O usuário salvou um envio.',
    'Tabela de notas visualizada',
    'O status da submissão foi visualizado.',
    'Formulário de submissão visualizado.'
}

moodle_3_events = {
    'Algum conteúdo foi publicado.',
    'Curso visto',
    'Discussão criada',
    'Discussão visualizada',
    'Módulo de curso criado',
    'Módulo do curso visualizado',
    'Post criado',
    'Entrada foi criada',
    'Entrada foi visualizada',
    'Lição finalizada',
    'Lição iniciada',
    'Página criada',
    'Página de conteúdo visualizada',
    'Questão criada',
    'Questão respondida',
    'Questão visualizada',
    'Resposta enviada',
    'Tentativa do questionário entregue',
    'Tentativa do questionário iniciada',
    'Tentativa do questionário visualizada'
}

from .moodle_events_config import moodle_1_events, moodle_3_events

apc_moodle_1 = {
    "path": "/Users/michaelsilva/Desktop/min/logs/apc/logs_Algoritmos_e_Programação_de_Computadores-2019_2.csv",
    "output": "/Users/michaelsilva/Desktop/min/logs/apc/output/apc-2019-2.csv",
    "teachers_by_name": {
        "Sinayra Pascoal Cotts Moreira": "2487",
        "Moisés Silva de Sousa": "48275",
        "Henrique Lopes": "3",
    },
    "teachers_by_id": {
        "2487": "Sinayra Pascoal Cotts Moreira",
        "48275": "Moisés Silva de Sousa",
        "3": "Henrique Lopes"
    },
    "events": moodle_1_events,
    "sep": ";",
    "date_from": "2019-07-01",
    "date_to": "2019-12-30",
    "fix_name": {
        "GUILHERME BRITO VILAS - BOAS": "GUILHERME BRITO VILAS BOAS",
        "DANIEL FERREIRA": "DANIEL FERREIRA PUTINI",
        "HUGO CALDAMURYS DIAS DE ARAGAO": "2487",
        "CARLOS MACIEL CARMO SANTOS": "CARLOS MACIEL DO CARMO SANTOS",
        "DANIEL ELIAS": "DANIEL ELIAS COSTA SANTOS",
        "WENGEL RODRIGUES": "WENGEL RODRIGUES FARIAS",
        "GABRIELE ALMEIDA": "GABRIELE ALMEIDA DOS SANTOS",
        "DAVI PIERRE DAVI GONCALVES AKEGAWA PIERRE": "DAVI GONCALVES AKEGAWA PIERRE",
        "ANDERSON COSTA": "ANDERSON DA COSTA",
        "YVES GUSTAVO RIBEIRO PIMENTA": "2487",
        "JOAO VITOR LOURENCO": "JOAO VITOR MARQUES LOURENCO",
        "JOAO CARLOS ALMEIDA": "JOAO CARLOS ALMEIDA RUNKEL",
        "LIZANDRA AZEVEDO": "LIZANDRA PEREIRA DE AZEVEDO",
        "BEATRIZ ANDRADE": "BEATRIZ FRANCA DE ANDRADE",
        "LARA G": "LARA GIULIANA LIMA DOS SANTOS",
        "ARTHUR HENRIQUE SANTOS MEIRA": "ARTHUR HENRIQUE DOS SANTOS MEIRA",
        "GABRIEL SOUZA": "GABRIEL DE SOUZA FONSECA RIBEIRO",
        "BRENNO OLIVEIRA SILVA": "BRENNO DA SILVA OLIVEIRA",
        "VICTOR CAVALCANTE RAMALHO": "VICTOR ANDRADE CAVALCANTE",
        "GUILHERME BRITO": "GUILHERME BRITO VILAS BOAS",
    }

}

apc_moodle_3 = {
    "path": "/Users/michaelsilva/Desktop/min/logs/apc/logs_Algoritmos_e_Programação_de_Computadores_Turmas A, B, C, D, E, G, H _20210515-1832.csv",
    "output": "/Users/michaelsilva/Desktop/min/logs/apc/output/apc-2020-1.csv",
    "teachers_by_name": {
        "Admin User": "2",
        "Letícia Lopes Leite": "4",
        "Henrique Lopes": "5",
        "Vinicius Ruela Pereira Borges": "2637",
        "Andre Costa Drummond": "20",
        "Carla Denise Castanho": "403",
        "Teofilo Emidio De Campos": "10",
        "Marcos Fagundes Caetano": "11",
        "Edison Ishikawa": "9",
        "Jeremias Moreira Gomes": "3422",
        "Guilherme Novaes Ramos": "1119",
        "Luis Paulo Faina Garcia": "1619",
        "Ricardo Pezzuol Jacobi": "2283",
        "Maristela Terto De Holanda": "1895",
    },
    "teachers_by_id": {
        "2487": "Sinayra Pascoal Cotts Moreira",
        "48275": "Moisés Silva de Sousa",
        "3": "Henrique Lopes",
        "2637": "Vinicius Ruela Pereira Borges",
        "20": "Andre Costa Drummond",
        "403": "Carla Denise Castanho",
        "10": "Teofilo Emidio De Campos",
        "11": "Marcos Fagundes Caetano",
        "9": "Edison Ishikawa",
        "3422": "Jeremias Moreira Gomes",
        "1119": "Guilherme Novaes Ramos",
        "1619": "Luis Paulo Faina Garcia",
        "2283": "Ricardo Pezzuol Jacobi",
        "1895": "Maristela Terto De Holanda"
    },
    "events": moodle_3_events,
    "sep": ",",
    "date_from": "2020-07-01",
    "date_to": "2020-12-30"
}

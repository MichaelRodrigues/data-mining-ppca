from .moodle_events_config import moodle_1_events, moodle_3_events

ics_moodle_1 = {
    "path": "/Users/michaelsilva/Desktop/min/logs/isc/csv/logs_ISC_2018_1.csv",
    "output": "//Users/michaelsilva/Desktop/min/logs/isc/output/isc-2018.csv",
    "teachers_by_name": {
        "Marcus Vinicius Lamar": "23210",
        "Moisés Silva de Sousa": "48275",
        "Henrique Lopes": "3",
        "@prender EaD": "2",
        "Vinicius Thiesen Lamar": "88962"
    },
    "teachers_by_id": {
        "23210": "Marcus Vinicius Lamar",
        "48275": "Moisés Silva de Sousa",
        "3": "Henrique Lopes",
        "2": "@prender EaD",
        "88962": "Vinicius Thiesen Lamar"
    },
    "events": moodle_1_events,
    "sep": ";",
    "date_from": "2019-07-01",
    "date_to": "2019-12-30"
}


from .moodle_events_config import moodle_1_events

ed_moodle_1 = {
    "path": "/Users/michaelsilva/Desktop/logs_Estruturas_de_Dados_2019_1.csv",
    "output": "/Users/michaelsilva/Desktop/output.csv",
    "teachers_by_name": {
        "GERALDO PEREIRA ROCHA FILHO": "82791",
        "Henrique Lopes": "3",
        "Moisés Silva de Sousa": "48275",
        "@prender EaD": "2"
    },
    "teachers_by_id": {
        "82791": "GERALDO PEREIRA ROCHA FILHO",
        "48275": "Moisés Silva de Sousa",
        "2": "@prender EaD",
        "3": "Henrique Lopes"
    },
    "events": moodle_1_events,
    "sep": ";",
    "date_from": "2019-07-01",
    "date_to": "2019-12-30"
}
from .moodle_events_config import moodle_1_events

oac_moodle_1 = {
    "path": "/Users/michaelsilva/Desktop/logs_Organizacao_e_Arquitetura_de_Computadores_2019_2.csv",
    "output": "/Users/michaelsilva/Desktop/output.csv",
    "teachers_by_name": {
        "Marcelo Grandi Mandelli": "57620",
        "Carla Koike": "24638",
        "Ricardo Pezzuol Jacobi": "23039",
        "Marcus Vinicius Lamar": "23210",
        "Henrique Lopes": "3",
        "Moisés Silva de Sousa": "48275",
        "@prender EaD": "2"
    },
    "teachers_by_id": {
        "57620": "Marcelo Grandi Mandelli",
        "24638": "Carla Koike",
        "23039": "Ricardo Pezzuol Jacobi",
        "23210": "Marcus Vinicius Lamar",
        "48275": "Moisés Silva de Sousa",
        "2": "@prender EaD",
        "3": "Henrique Lopes"
    },
    "events": moodle_1_events,
    "sep": ";",
    "date_from": "2019-07-01",
    "date_to": "2019-12-30"
}
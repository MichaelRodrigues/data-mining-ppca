import pandas as pd
import time
from handle_data import handle_data
from config.config import current_config
from util.transform_util import transform_datetime

if __name__ == '__main__':

    start = time.time()

    print("start: ", time.strftime('%H:%M:%S', time.localtime()))

    # read file
    df = pd.read_csv(current_config["path"], engine='python', sep=current_config["sep"])

    print("read file: ", (time.time() - start), " seconds")

    # fix time
    df['datetime'] = df.apply(lambda row: transform_datetime(row, '%d/%m/%Y %H:%M', '%Y-%m-%d %H:%M'), axis=1)

    # filter by time
    df_date = df[(df['datetime'] > current_config["date_from"]) & (df['datetime'] < current_config["date_to"])]

    print("filter by time: ", (time.time() - start), " seconds")

    print("start extract and transform")

    # extract and transform data
    events_df = df_date[df_date['Nome do evento'].isin(current_config["events"])].copy()
    result_df = events_df.apply(handle_data, axis=1)

    print("end extract and transform: ", (time.time() - start), " seconds")

    # save result
    result_header = ['datetime', 'user_id', 'user_name', 'user_profile', 'action', 'object', 'object_id', 'user_name_code']
    result_df.to_csv(current_config["output"], columns=result_header, index=False)

    print("TOTAL TIME: ", str((time.time() - start)/60), " minutes")

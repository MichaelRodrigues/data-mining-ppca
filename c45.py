from statistics import mean

import pandas as pd
import time

from numpy.core import std
from sklearn import metrics
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, KFold
import matplotlib.pyplot as plt
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.naive_bayes import GaussianNB
from c45 import C45

if __name__ == '__main__':
    start = time.time()
    path_prepared = '/Users/michaelsilva/Desktop/prepared.csv'

    print("C4.5 start: ", time.strftime('%H:%M:%S', time.localtime()))

    # read prepared
    df = pd.read_csv(path_prepared, engine='python', sep=',')

    print("read file: ", (time.time() - start), " seconds")

    features = [
        'readtask',
        'readcourse',
        'readfile',
        'readurl',
        'sentfile',
        'readfolder',
        'createpost',
        'createcomment',
        'readpage',
        'readpost',
        'readforum',
        'savefile'
    ]

    c45 = C45()

    X = df[features]
    y = df['abandonment']

    # implementing train-test-split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=int(time.time()))

    c45.fit(X_train, y_train)


    # start kfold
    cv = KFold(n_splits=10, random_state=1, shuffle=True)

    # evaluate model
    scores = cross_val_score(c45, X, y, scoring='accuracy', cv=cv, n_jobs=-1, error_score='raise')
    # report performance
    print('Accuracy: %.3f (%.3f)' % (mean(scores), std(scores)))
    # end kfold

    print("TOTAL TIME: ", str(time.time() - start), " seconds")
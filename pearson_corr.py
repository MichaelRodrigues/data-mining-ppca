import pandas as pd
import time


def count_criteria(user_id, action, obj):
    total = len(
        df_users[(df_users['action'] == action) &
                 (df_users['object'] == obj)])
    return len(
        df_users[(df_users['user_id'] == user_id) &
                 (df_users['action'] == action) &
                 (df_users['object'] == obj)])/total


def get_abandonment(grade):
    if grade == 'SR' or grade == 'TR':
        return 1
    return 0


if __name__ == '__main__':
    start = time.time()
    path_users = '/Users/michaelsilva/Desktop/min/logs/apc/output/apc-2019-2.csv'
    path_grades = '/Users/michaelsilva/Desktop/grades.csv'

    print("start: ", time.strftime('%H:%M:%S', time.localtime()))

    # read users
    df_users = pd.read_csv(path_users, engine='python', sep=',')

    # read grades
    df_grades = pd.read_csv(path_grades, engine='python', sep=',')

    print("read file: ", (time.time() - start), " seconds")

    grades_by_code = df_grades.set_index('name_code').T.to_dict('dict')

    students = []

    # TODO: improve to python algorithm
    for user_name_code in list(df_users["user_name_code"].drop_duplicates()):
        if user_name_code in grades_by_code:
            user = df_users[(df_users['user_name_code'] == user_name_code)].iloc[0]

            data = {
                'user_id': user['user_id'],
                'user_name': user['user_name'],
                'code': grades_by_code[user_name_code]['code'],
                'readtask': count_criteria(user['user_id'], 'read', 'task'),
                'readcourse': count_criteria(user['user_id'], 'read', 'course'),
                'readfile': count_criteria(user['user_id'], 'read', 'file'),
                'readurl': count_criteria(user['user_id'], 'read', 'url'),
                'readforum': count_criteria(user['user_id'], 'read', 'forum'),
                'readpost': count_criteria(user['user_id'], 'read', 'post'),
                'readpage': count_criteria(user['user_id'], 'read', 'page'),
                'createcomment': count_criteria(user['user_id'], 'create', 'comment'),
                'createpost': count_criteria(user['user_id'], 'create', 'post'),
                'savefile': count_criteria(user['user_id'], 'save', 'file'),
                'sentfile': count_criteria(user['user_id'], 'sent', 'file'),
                'createfile': count_criteria(user['user_id'], 'create', 'file'),
                'createlabel': count_criteria(user['user_id'], 'create', 'label'),
                'createurl': count_criteria(user['user_id'], 'create', 'url'),
                'createtask': count_criteria(user['user_id'], 'create', 'task'),
                'readfolder': count_criteria(user['user_id'], 'read', 'folder'),
                'createother': count_criteria(user['user_id'], 'create', 'other'),
                'createpage': count_criteria(user['user_id'], 'create', 'page'),
                'grade': grades_by_code[user_name_code]['grade'],
                'absences': grades_by_code[user_name_code]['absences'],
                'abandonment': get_abandonment(grades_by_code[user_name_code]['grade'])
            }
            students.append(data)

    df = pd.DataFrame(students)
    df.corr().to_csv('/Users/michaelsilva/Desktop/corr.csv', index=False)

    print("TOTAL TIME: ", str(time.time() - start), " seconds")

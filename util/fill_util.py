import unidecode

from config.config import current_config
from .extract_util import extract_in_description
import re


def fill_user(row, column):
    row['user_name'] = unidecode.unidecode(row[column]).upper()

    if 'fix_name' in current_config and row['user_name'] in current_config['fix_name']:
        row['user_name'] = current_config['fix_name'][row['user_name']]

    if row[column] in current_config['teachers_by_name']:
        row['user_id'] = current_config['teachers_by_name'][row[column]]
        row['user_profile'] = 'teacher'
    else:
        row['user_id'] = extract_in_description(row, 0)
        row['user_profile'] = 'student'

    row['user_name_code'] = unidecode.unidecode(re.sub(r"\s+", "", row['user_name'], flags=re.UNICODE)).upper()


def fill_user_full_name(row):
    fill_user(row, 'Nome completo')


def fill_user_affected(row):
    fill_user(row, 'Usuário afetado')


def fill_action(row, action, obj):
    row['action'] = action
    row['object'] = obj


def fill_object_id(row, index):
    row['object_id'] = extract_in_description(row, index)


def fill_action_object_id(row, action, obj, index):
    fill_action(row, action, obj)
    fill_object_id(row, index)


def fill_with_full_name(row, action, obj, obj_index=1):
    fill_user_full_name(row)
    fill_action_object_id(row, action, obj, obj_index)


def fill_with_affected(row, action, obj, obj_index=1):
    fill_user_affected(row)
    fill_action_object_id(row, action, obj, obj_index)

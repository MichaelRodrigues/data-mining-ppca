from datetime import datetime


def transform_datetime(df_row, format_from, format_to):
    return datetime.strptime(df_row['Hora'], format_from).strftime(format_to)

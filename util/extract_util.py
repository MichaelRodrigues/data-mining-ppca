import re


def extract_in_description(row, index):
    return re.findall(r"'([\s\d]+)'", row['Descrição'], re.DOTALL)[index]

from statistics import mean
import pandas as pd
import time
from numpy.core import std
from sklearn.model_selection import train_test_split, KFold
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import GaussianNB

if __name__ == '__main__':
    start = time.time()
    path_prepared = '/Users/michaelsilva/Desktop/prepared.csv'

    print("Naive Bayes start: ", time.strftime('%H:%M:%S', time.localtime()))

    # read prepared
    df = pd.read_csv(path_prepared, engine='python', sep=',')

    print("read file: ", (time.time() - start), " seconds")

    features = [
        'readtask',
        'readcourse',
        'readfile',
        'readurl',
        'sentfile',
        'readfolder',
        'createpost',
        'createcomment',
        'readpage',
        'readpost',
        'readforum',
        'savefile'
    ]

    X = df[features]
    y = df['abandonment']

    # implementing train-test-split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=int(time.time()))

    gnb = GaussianNB()

    # Train classifier
    gnb.fit(X_train, y_train)

    # start kfold
    cv = KFold(n_splits=10, random_state=1, shuffle=True)

    # evaluate model
    scores = cross_val_score(gnb, X, y, scoring='accuracy', cv=cv, n_jobs=-1)
    # report performance
    print('Accuracy: %.3f (%.3f)' % (mean(scores), std(scores)))
    # end kfold

    print("TOTAL TIME: ", str(time.time() - start), " seconds")

import pandas as pd
import time
from datetime import datetime
import numpy as np

weekdays = [
    'Seg',
    'Ter',
    'Qua',
    'Qui',
    'Sex',
    'Sab',
    'Dom'
]


def get_weekday(row):
    date = datetime.strptime(row['datetime'], '%Y-%m-%d %H:%M')
    return weekdays[date.weekday()]


def get_hour(row):
    date = datetime.strptime(row['datetime'], '%Y-%m-%d %H:%M')
    return date.hour


def get_totals(w, i=0):
    data = {
        'hour': i,
        'weekday': w,
        'total': len(students[(students['hour'] == i) & (students['weekday'] == w)])
    }
    return data


if __name__ == '__main__':
    start = time.time()
    path = '/Users/michaelsilva/Desktop/min/logs/apc/output/apc-2019-2.csv'

    print("start: ", time.strftime('%H:%M:%S', time.localtime()))

    # read file
    df = pd.read_csv(path, engine='python', sep=',')

    print("read file: ", (time.time() - start), " seconds")

    students = df[df['user_profile'].isin(['student'])].copy()
    students['weekday'] = students.apply(lambda row: get_weekday(row), axis=1)
    students['hour'] = students.apply(lambda row: get_hour(row), axis=1)
    table = list(np.concatenate([[get_totals(w, h) for w in weekdays] for h in range(24)]).ravel())

    occurrences = pd.DataFrame(table, columns=['hour', 'weekday', 'total'])
    occurrences.to_csv('/Users/michaelsilva/Desktop/occurrences--20.csv', index=False)

    print("TOTAL TIME: ", str(time.time() - start), " seconds")

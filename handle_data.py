from util.fill_util import fill_with_affected, fill_with_full_name


def any_content(row):
    fill_with_full_name(row, 'create', 'comment')
    return row


def comment_created(row):
    fill_with_full_name(row, 'create', 'comment')
    return row


def comment_viewed(row):
    fill_with_affected(row, 'read', 'comment')
    return row


def course_viewed(row):
    fill_with_full_name(row, 'read', 'course')
    return row


def discussion_created(row):
    fill_with_full_name(row, 'create', 'post')
    return row


def discussion_viewed(row):
    fill_with_full_name(row, 'read', 'post')
    return row


def form_grade_viewed(row):
    fill_with_affected(row, 'read', 'task', 2)
    return row


def form_submission_viewed(row):
    fill_with_affected(row, 'read', 'task')
    return row


def module_created(row):
    obj = 'other'
    context = str(row['Contexto do Evento'])

    if 'Arquivo' in context:
        obj = 'file'
    elif 'URL' in context:
        obj = 'url'
    elif 'Tarefa' in context:
        obj = 'task'
    elif 'Página' in context:
        obj = 'page'
    elif 'Rótulo' in context:
        obj = 'label'
    elif 'Lição' in context:
        obj = 'task'
    elif 'Ferramenta' in context:
        obj = 'url'
    elif 'Questionário' in context:
        obj = 'task'
    elif 'Pesquisa' in context:
        obj = 'task'
    elif 'Atividade' in context:
        obj = 'task'

    fill_with_full_name(row, 'create', obj)
    return row


def module_viewed(row):
    obj = 'other'
    component = row['Componente']

    if 'Arquivo' in component:
        obj = 'file'
    if 'Planilha' in component:
        obj = 'file'
    elif 'URL' in component:
        obj = 'url'
    elif 'Fórum' in component:
        obj = 'forum'
    elif 'Página' in component:
        obj = 'page'
    elif 'Pasta' in component:
        obj = 'folder'
    elif 'Diário' in component:
        obj = 'diary'
    elif 'Lição' in component:
        obj = 'task'
    elif 'Ferramenta' in component:
        obj = 'url'
    elif 'Questionário' in component:
        obj = 'task'
    elif 'Pesquisa' in component:
        obj = 'task'
    elif 'Atividade' in component:
        obj = 'task'

    fill_with_full_name(row, 'read', obj)
    return row


def post_created(row):
    fill_with_full_name(row, 'create', 'post')
    return row


def sent_task(row):
    fill_with_full_name(row, 'sent', 'task')
    return row


def submission_created(row):
    fill_with_full_name(row, 'create', 'file', 2)
    return row


def file_sent(row):
    fill_with_full_name(row, 'sent', 'file')
    return row


def save_file(row):
    fill_with_affected(row, 'save', 'file', 2)
    return row


def grade_viewed(row):
    fill_with_full_name(row, 'read', 'task')
    return row


def submission_viewed(row):
    # viewed
    fill_with_full_name(row, 'read', 'task')
    return row


def entry_created(row):
    fill_with_full_name(row, 'create', 'task')
    return row


def entry_viewed(row):
    fill_with_full_name(row, 'read', 'task')
    return row


def lesson_ended(row):
    fill_with_full_name(row, 'sent', 'task')
    return row


def lesson_start(row):
    fill_with_full_name(row, 'start', 'task')
    return row


def page_created(row):
    fill_with_full_name(row, 'create', 'task')
    return row


def page_viewed(row):
    fill_with_full_name(row, 'read', 'task')
    return row


def question_created(row):
    fill_with_full_name(row, 'create', 'task')
    return row


def question_answered(row):
    fill_with_full_name(row, 'sent', 'task')
    return row


def question_viewed(row):
    fill_with_full_name(row, 'read', 'task')
    return row


def answers_sent(row):
    fill_with_full_name(row, 'sent', 'task')
    return row


def try_given(row):
    fill_with_affected(row, 'sent', 'task')
    return row


def try_started(row):
    fill_with_affected(row, 'start', 'task')
    return row


def try_viewed(row):
    fill_with_affected(row, 'read', 'task')
    return row


handles = {
    'Algum conteúdo foi publicado.': any_content,
    'Comentário criado': comment_created,
    'Comentário visualizado': comment_viewed,
    'Curso visto': course_viewed,
    'Discussão criada': discussion_created,
    'Discussão visualizada': discussion_viewed,
    'Formulário de notas visualizado': form_grade_viewed,
    'Formulário de submissão visualizado.': form_submission_viewed,
    'Módulo de curso criado': module_created,
    'Módulo do curso visualizado': module_viewed,
    'Post criado': post_created,
    'Um envio foi submetido.': sent_task,
    'Submissão criada.': submission_created,
    'Um arquivo foi enviado.': file_sent,
    'O usuário salvou um envio.': save_file,
    'Tabela de notas visualizada': grade_viewed,
    'O status da submissão foi visualizado.': submission_viewed,
    'Entrada foi criada': entry_created,
    'Entrada foi visualizada': entry_viewed,
    'Lição finalizada': lesson_ended,
    'Lição iniciada': lesson_start,
    'Página criada': page_created,
    'Página de conteúdo visualizada': page_viewed,
    'Questão criada': question_created,
    'Questão respondida': question_answered,
    'Questão visualizada': question_viewed,
    'Resposta enviada': answers_sent,
    'Tentativa do questionário entregue': try_given,
    'Tentativa do questionário iniciada': try_started,
    'Tentativa do questionário visualizada': try_viewed,
    'Relatório do questionário visualizado': module_viewed,
}


def handle_data(row):
    try:
        return handles[row['Nome do evento']](row)
    except Exception:
        print("Error in line: ", row)
        return row

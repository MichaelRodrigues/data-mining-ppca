from statistics import mean

import numpy as np
import pandas as pd
import time
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split

if __name__ == '__main__':
    start = time.time()
    path_prepared = '/Users/michaelsilva/Desktop/prepared.csv'
    path_predict = '/Users/michaelsilva/Desktop/predict-20.csv'

    print("start: ", time.strftime('%H:%M:%S', time.localtime()))

    # read prepared
    df = pd.read_csv(path_prepared, engine='python', sep=',')

    # read predict
    df_predict = pd.read_csv(path_predict, engine='python', sep=',')

    print("read file: ", (time.time() - start), " seconds")

    features = [
        'readtask',
        'readcourse',
        'readfile',
        'readurl',
        'sentfile',
        'readfolder',
        'createpost',
        'createcomment',
        'readpage',
        'readpost',
        'readforum',
        'savefile'
    ]

    X = df[features]
    y = df['abandonment']

    for i in range(20):
        # implementing train-test-split
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=66)

        # random forest model creation
        rfc = RandomForestClassifier()
        rfc.fit(X_train, y_train)

        # start predict
        # 0 => Não abandonou
        df_real = pd.DataFrame(df_predict, columns=features)

        yhat = rfc.predict(df_real)
        print(str(np.count_nonzero(yhat == 1)/np.count_nonzero(yhat == 0)).replace(',', '.'))
    # end predict

    print("TOTAL TIME: ", str(time.time() - start), " seconds")

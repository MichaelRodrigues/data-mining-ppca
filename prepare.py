import pandas as pd
import time


def count_criteria(user_id, action, obj):
    total = len(
        df_users[(df_users['action'] == action) &
                 (df_users['object'] == obj)])
    target = len(
        df_users[(df_users['user_id'] == user_id) &
                 (df_users['action'] == action) &
                 (df_users['object'] == obj)])

    if total != 0 and target != 0:
        return target/total
    else:
        return 0


def get_abandonment(grade):
    if grade == 'SR' or grade == 'TR':
        return 1
    return 0


if __name__ == '__main__':
    start = time.time()
    path_users = '/Users/michaelsilva/Desktop/min/logs/apc/output/apc-2019-2.csv'
    path_grades = '/Users/michaelsilva/Desktop/grades.csv'

    print("start: ", time.strftime('%H:%M:%S', time.localtime()))

    # read users
    df_users = pd.read_csv(path_users, engine='python', sep=',')

    # read grades
    df_grades = pd.read_csv(path_grades, engine='python', sep=',')

    print("read file: ", (time.time() - start), " seconds")

    grades_by_code = df_grades.set_index('name_code').T.to_dict('dict')

    students = []

    # TODO: improve to python algorithm
    for user_name_code in list(df_users["user_name_code"].drop_duplicates()):
        if user_name_code in grades_by_code:
            user = df_users[(df_users['user_name_code'] == user_name_code)].iloc[0]

            data = {
                'user_id': user['user_id'],
                'user_name': user['user_name'],
                'code': grades_by_code[user_name_code]['code'],
                'readtask': count_criteria(user['user_id'], 'read', 'task'),
                'readcourse': count_criteria(user['user_id'], 'read', 'course'),
                'readfile': count_criteria(user['user_id'], 'read', 'file'),
                'readurl': count_criteria(user['user_id'], 'read', 'url'),
                'readforum': count_criteria(user['user_id'], 'read', 'forum'),
                'sentfile': count_criteria(user['user_id'], 'read', 'file'),
                'readfolder': count_criteria(user['user_id'], 'read', 'folder'),
                'createpost': count_criteria(user['user_id'], 'read', 'post'),
                'createcomment': count_criteria(user['user_id'], 'read', 'comment'),
                'readpage': count_criteria(user['user_id'], 'read', 'page'),
                'savefile': count_criteria(user['user_id'], 'read', 'file'),
                'readpost': count_criteria(user['user_id'], 'read', 'post'),
                'grade': grades_by_code[user_name_code]['grade'],
                'absences': grades_by_code[user_name_code]['absences'],
                'abandonment': get_abandonment(grades_by_code[user_name_code]['grade'])
            }
            students.append(data)

    columns = [
        'user_id',
        'user_name',
        'code',
        'readtask',
        'readcourse',
        'readfile',
        'readurl',
        'sentfile',
        'readfolder',
        'createpost',
        'createcomment',
        'readpage',
        'readpost',
        'readforum',
        'savefile',
        'grade',
        'absences',
        'abandonment'
    ]

    df = pd.DataFrame(students, columns=columns)
    df.to_csv('/Users/michaelsilva/Desktop/prepared.csv', index=False)

    print("TOTAL TIME: ", str(time.time() - start), " seconds")

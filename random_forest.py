from statistics import mean

import pandas as pd
import time

from numpy.core import std
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.model_selection import train_test_split, KFold
from sklearn.model_selection import cross_val_score
import matplotlib.pyplot as plt

if __name__ == '__main__':
    start = time.time()
    path_prepared = '/Users/michaelsilva/Desktop/prepared.csv'

    print("Random Forest. start: ", time.strftime('%H:%M:%S', time.localtime()))

    # read prepared
    df = pd.read_csv(path_prepared, engine='python', sep=',')

    print("read file: ", (time.time() - start), " seconds")

    features = [
        'readtask',
        'readcourse',
        'readfile',
        'readurl',
        'sentfile',
        'readfolder',
        'createpost',
        'createcomment',
        'readpage',
        'readpost',
        'readforum',
        'savefile'
    ]

    X = df[features]
    y = df['abandonment']

    # implementing train-test-split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=66)

    # random forest model creation
    rfc = RandomForestClassifier()
    rfc.fit(X_train, y_train)

    # start roc_auc
    # predictions
    rfc_predict = rfc.predict(X_test)

    rfc_cv_score = cross_val_score(rfc, X, y, cv=10, scoring='roc_auc')
    conf = confusion_matrix(y_test, rfc_predict)

    print("=== Confusion Matrix ===")
    print(conf)
    print('\n')
    print("=== Classification Report ===")
    print(classification_report(y_test, rfc_predict))
    print('\n')
    print("=== All AUC Scores ===")
    print(rfc_cv_score)
    print('\n')
    print("=== Mean AUC Score ===")
    print("Mean AUC Score - Random Forest: ", rfc_cv_score.mean())

    plt.imshow(conf, cmap='binary', interpolation='None')
    plt.show()
    # end roc_auc

    # start kfold
    cv = KFold(n_splits=10, random_state=1, shuffle=True)

    # evaluate model
    scores = cross_val_score(rfc, X, y, scoring='accuracy', cv=cv, n_jobs=-1)
    # report performance
    print('Accuracy: %.3f (%.3f)' % (mean(scores), std(scores)))
    # end kfold

    # fig, axes = plt.subplots(nrows=1, ncols=5, figsize=(10, 2), dpi=900)
    # for index in range(0, 5):
    #     tree.plot_tree(rfc.estimators_[index],
    #                    feature_names=features,
    #                    class_names=["NAO", "SIM"],
    #                    filled=True,
    #                    ax=axes[index])
    #
    #     axes[index].set_title('Estimator: ' + str(index), fontsize=11)
    # fig.savefig('rf_5trees.png')

    # # start predict
    # # 0 => Não abandonou
    # real = {
    #     'readtask': [0, 0],
    #     'readcourse': [0.006272157076629398, 0.004158712844286883],
    #     'readfile': [0.005568814638027049, 0.003889330858304605],
    #     'readurl': [0.008374875373878364, 0.006380857427716849],
    #     'readforum': [0, 0.007537688442211055],
    #     'readpost': [0, 0.009475679090334808]
    # }
    # df_real = pd.DataFrame(real, columns=features)
    #
    # yhat = rfc.predict(df_real)
    # print('Predicted Class: ', yhat)
    # # end predict

    print("TOTAL TIME: ", str(time.time() - start), " seconds")

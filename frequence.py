import pandas as pd
import time

if __name__ == '__main__':
    start = time.time()
    path = '/Users/michaelsilva/Desktop/min/logs/apc/output/apc-2020-1.csv'

    print("start: ", time.strftime('%H:%M:%S', time.localtime()))

    # read file
    df = pd.read_csv(path, engine='python', sep=',')

    print("read file: ", (time.time() - start), " seconds")

    students = df[df['user_profile'].isin(['student'])].copy()
    students["events"] = students["action"] + '/' + students["object"]
    students['frequency'] = students['events'].map(students['events'].value_counts(normalize=True))

    print(students['events'].value_counts(normalize=True).mul(100).round(1).astype(str) + '%')

    frequencies = students.drop_duplicates('events')

    frequencies.to_csv('/Users/michaelsilva/Desktop/freqs2.csv', columns=['events', 'frequency'], index=False)

    print("TOTAL TIME: ", str(time.time() - start), " seconds")

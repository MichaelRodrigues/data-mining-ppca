from statistics import mean

import pandas as pd
import time

from numpy.core import std
from sklearn import metrics, tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split, KFold
import matplotlib.pyplot as plt
from sklearn.model_selection import cross_val_score
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.naive_bayes import GaussianNB
from sklearn.tree import DecisionTreeClassifier

if __name__ == '__main__':
    start = time.time()
    path_prepared = '/Users/michaelsilva/Desktop/prepared.csv'

    print("start: ", time.strftime('%H:%M:%S', time.localtime()))

    # read prepared
    df = pd.read_csv(path_prepared, engine='python', sep=',')

    print("read file: ", (time.time() - start), " seconds")

    features = ['readtask', 'readcourse', 'readfile', 'readurl', 'readforum', 'readpost']

    X = df[features]
    y = df['abandonment']

    # implementing train-test-split
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=int(time.time()))

    dtree = DecisionTreeClassifier()

    # Train classifier
    dtree.fit(X_train, y_train)

    fig, axes = plt.subplots(nrows=1, ncols=1, figsize=(5, 4), dpi=300)
    tree.plot_tree(dtree,
                   feature_names=features,
                   class_names=['Não Abandonou', 'Abandonou'],
                   filled=True)
    fig.savefig('imagename.png')

    print("TOTAL TIME: ", str(time.time() - start), " seconds")
